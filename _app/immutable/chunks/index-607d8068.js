import{O as l}from"./index-afce469b.js";function $(t){return t<.5?4*t*t*t:.5*Math.pow(2*t-2,3)+1}function m(t){const o=t-1;return o*o*o+1}function g(t,{delay:o=0,duration:s=400,easing:c=l}={}){const r=+getComputedStyle(t).opacity;return{delay:o,duration:s,easing:c,css:a=>`opacity: ${a*r}`}}function x(t,{delay:o=0,duration:s=400,easing:c=m,x:r=0,y:a=0,opacity:e=0}={}){const n=getComputedStyle(t),f=+n.opacity,p=n.transform==="none"?"":n.transform,u=f*(1-e);return{delay:o,duration:s,easing:c,css:(y,i)=>`
			transform: ${p} translate(${(1-y)*r}px, ${(1-y)*a}px);
			opacity: ${f-u*i}`}}function C(t,{delay:o=0,duration:s=400,easing:c=m,start:r=0,opacity:a=0}={}){const e=getComputedStyle(t),n=+e.opacity,f=e.transform==="none"?"":e.transform,p=1-r,u=n*(1-a);return{delay:o,duration:s,easing:c,css:(y,i)=>`
			transform: ${f} scale(${1-p*i});
			opacity: ${n-u*i}
		`}}export{x as a,$ as c,g as f,C as s};
